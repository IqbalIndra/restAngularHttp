package com.tester.springrest.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.tester.springrest.dao.CustomerDAO;
import com.tester.springrest.model.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
	private static List<Customer> customers;
	{
		customers = new ArrayList<>();
		for (int i = 1; i <= 5; i++) {
			Customer customer = new Customer();
			customer.setId(i);
			customer.setFirstName("Customer " + i);
			customer.setLastName("Lastname " + i);
			customer.setEmail("customer" + i + "@gmail.com");
			customers.add(customer);
		}
	}

	@Override
	public List<Customer> getListCustomer() {
		// TODO Auto-generated method stub
		return customers;
	}

	@Override
	public Customer getCustomerByID(int id) {
		// TODO Auto-generated method stub
		for (Customer customer : customers) {
			if (customer.getId() == id)
				return customer;
		}
		return null;
	}

	@Override
	public Customer addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		customers.add(customer);
		return customer;
	}

	@Override
	public Customer updateCustomer(Customer oldCustomer) {
		// TODO Auto-generated method stub
		Customer customer = getCustomerByID(oldCustomer.getId());
		if(customer != null){
			int index = customers.indexOf(customer);
			customers.set(index, oldCustomer);
			return oldCustomer;
		}
		return null;
	}

	@Override
	public int deleteCustomer(int id) {
		// TODO Auto-generated method stub
		int index = 0;
		Customer oldCustomer = getCustomerByID(id);
		if (oldCustomer != null) {
			customers.remove(oldCustomer);
			index = 1;
		}
		return index;
	}

}
