package com.tester.springrest.dao;

import java.util.List;

import com.tester.springrest.model.Customer;

public interface CustomerDAO {
	List<Customer> getListCustomer();
	Customer getCustomerByID(int id);
	Customer addCustomer(Customer customer);
	Customer updateCustomer(Customer oldCustomer);
	int deleteCustomer(int id);
}
