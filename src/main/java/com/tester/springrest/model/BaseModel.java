package com.tester.springrest.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public abstract class BaseModel {
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date creationDate;
	
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date editDate;
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getEditDate() {
		return editDate;
	}
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	
}
