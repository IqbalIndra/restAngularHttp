package com.tester.springrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tester.springrest.model.Customer;
import com.tester.springrest.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("/customers")
	public List<Customer> getListCustomer() {
		return customerService.getListCustomer();
	}

	@GetMapping("/customers/{id}")
	public ResponseEntity getCustomerByID(@PathVariable("id") int id) {
		Customer customer = customerService.getCustomerByID(id);
		if (customer == null)
			return new ResponseEntity<>("No Customer found for id " + id, HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(customer, HttpStatus.OK);
	}

	@PostMapping("/customers")
	public ResponseEntity addCustomer(@RequestBody Customer customer) {
		customerService.addCustomer(customer);
		return new ResponseEntity<>(customer, HttpStatus.OK);
	}

	@PutMapping("/customers/{id}")
	public ResponseEntity updateCustomer(@PathVariable("id") int id, @RequestBody Customer customer) {
		Customer customerExist = customerService.updateCustomer(customer);
		if (null == customerExist)
			return new ResponseEntity<>("No Customer found for id " + id, HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(customerExist, HttpStatus.OK);

	}

	@DeleteMapping("/customers/{id}")
	public ResponseEntity deleteCustomer(@PathVariable("id") int id) {
		int idx = customerService.deleteCustomer(id);
		if (idx == 0)
			return new ResponseEntity<>("No Customer found for id " + id, HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}
