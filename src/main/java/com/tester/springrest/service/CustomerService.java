package com.tester.springrest.service;

import java.util.List;

import com.tester.springrest.model.Customer;

public interface CustomerService {
	List<Customer> getListCustomer();
	Customer getCustomerByID(int id);
	Customer addCustomer(Customer customer);
	Customer updateCustomer(Customer oldCustomer);
	int deleteCustomer(int id);
}
