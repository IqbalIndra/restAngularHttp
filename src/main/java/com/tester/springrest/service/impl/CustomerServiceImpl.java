package com.tester.springrest.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tester.springrest.dao.CustomerDAO;
import com.tester.springrest.model.Customer;
import com.tester.springrest.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	private CustomerDAO customerDAO;
	
	@Autowired
	public CustomerServiceImpl(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	@Override
	public List<Customer> getListCustomer() {
		// TODO Auto-generated method stub
		return customerDAO.getListCustomer();
	}

	@Override
	public Customer getCustomerByID(int id) {
		// TODO Auto-generated method stub
		return customerDAO.getCustomerByID(id);
	}

	@Override
	public Customer addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return customerDAO.addCustomer(customer);
	}

	@Override
	public Customer updateCustomer(Customer oldCustomer) {
		// TODO Auto-generated method stub
		return customerDAO.updateCustomer(oldCustomer);
	}

	@Override
	public int deleteCustomer(int id) {
		// TODO Auto-generated method stub
		return customerDAO.deleteCustomer(id);
	}

}
