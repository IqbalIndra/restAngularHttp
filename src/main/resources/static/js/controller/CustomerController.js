'use strict';
 
angular.module('springrestApp').controller('CustomerController', ['$scope', 'CustomerService', function($scope, CustomerService) {
    var self = this;
    self.customer={id:null,firstName:'',lastName:'',email:''};
    self.customers=[];
 
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;

 
 
    fetchAllCustomers();
 
    function fetchAllCustomers(){
        CustomerService.fetchAllCustomers()
            .then(
            function(d) {
                self.customers = d;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function addCustomer(customer){
    	CustomerService.addCustomer(customer)
            .then(
            fetchAllCustomers,
            function(errResponse){
                console.error('Error while creating User', errResponse);
            }
        );
    }
 
    function updateCustomer(customer, id){
    	CustomerService.updateCustomer(customer, id)
            .then(
            fetchAllCustomers,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteCustomer(id){
        CustomerService.deleteCustomer(id)
            .then(
            fetchAllCustomers,
            function(errResponse){
                console.error('Error while deleting User', errResponse);
            }
        );
    }
 
    function submit() {
        if(self.customer.id===null){
            console.log('Saving New User', self.customer);
            addCustomer(self.customer);
        }else{
            updateCustomer(self.customer, self.customer.id);
            console.log('User updated with id ', self.customer.id);
        }
        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.customers.length; i++){
            if(self.customers[i].id === id) {
                self.customer = angular.copy(self.customers[i]);
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.customer.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteCustomer(id);
    }
 
 
    function reset(){
        self.customer={id:null,firstName:'',lastName:'',email:''};
        $scope.myForm.$setPristine(); //reset Form
    }
 
}]);