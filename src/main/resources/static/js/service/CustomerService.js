'use strict';
angular.module('springrestApp').factory('CustomerService',['$http','$q', function($http, $q){
	var CUSTOMER_REST_SERVICE_URI = 'http://localhost:8080/springrest/customers';
	
	var factory = {
			fetchAllCustomers : fetchAllCustomers,
			addCustomer : addCustomer,
			updateCustomer : updateCustomer,
			deleteCustomer : deleteCustomer
	};
	
	return factory;
	
	function fetchAllCustomers(){
		var deferred = $q.defer();
		$http.get(CUSTOMER_REST_SERVICE_URI).then(
			function(response){
				deferred.resolve(response.data);
			},
			function(errResponse){
				console.error('Error while Fetching data ..');
				deferred.reject(errResponse);
			}
		);
		return deferred.promise;
	}
	
	function addCustomer(customer){
		var deferred = $q.defer();
		console.error(customer);
		$http.post(CUSTOMER_REST_SERVICE_URI, customer).then(
			function(response){
				deferred.resolve(response.data);
			}, 
			function(errResponse){
				console.error('Error while creating data ..');
				deferred.reject(errResponse);
			}
		);
		return deferred.promise;
	}
	
	function updateCustomer(customer,id){
		var deferred = $q.defer();
		$http.put(CUSTOMER_REST_SERVICE_URI+'/'+id, customer).then(
			function(response){
				deferred.resolve(response.data);
			},
			function(errResponse){
				console.error('Error while updating data ..');
				deferred.reject(errResponse);
			}
		);
		return deferred.promise;
	}
	
	function deleteCustomer(id) {
        var deferred = $q.defer();
        $http.delete(CUSTOMER_REST_SERVICE_URI+'/'+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User-->'+errResponse);
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}]);